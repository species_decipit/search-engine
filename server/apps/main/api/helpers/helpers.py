# -*- coding: utf-8 -*-

from django.conf import settings
from rest_framework.request import Request


def create_media_link(request: Request, path: str) -> str:
    return f'http://{request.META["HTTP_HOST"]}{settings.MEDIA_URL}{path}'
