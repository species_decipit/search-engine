# -*- coding: utf-8 -*-

from typing import List

from django.conf import settings
from rest_framework import decorators
from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response

from server.apps.main.data_provider.data_provider import (
    restore_inverted_index,
    restore_prefix_tree,
    restore_soundex_index,
)
from server.apps.main.logic.search.binary_search import search
from server.apps.main.api.helpers.helpers import create_media_link


@decorators.api_view(['GET'])
def binary_search(request: Request) -> Response:
    """API view performs binary_search.

    Args:
        request: Instance of Request

    Returns:
        instance of of Response with search results

    """
    query: str = request.GET.get('query', '')

    inverted_index = restore_inverted_index(settings.PATH_INVERTED_INDEX_DUMP)
    prefix_tree = restore_prefix_tree(settings.PATH_PREFIX_TREE_DUMP)
    inverted_prefix_tree = restore_prefix_tree(
        settings.PATH_INVERTED_PREFIX_TREE_DUMP
    )
    soundex_index = restore_soundex_index(settings.PATH_SOUNDEX_INDEX_DUMP)

    docs_ids: List[int] = search(
        inverted_index, prefix_tree, inverted_prefix_tree, soundex_index, query
    )

    return Response(
        data=[
            str(
                create_media_link(
                    request, settings.DOCUMENT_PATH / str(doc_id)
                )
            )
            for doc_id in docs_ids
        ],
        status=status.HTTP_200_OK,
    )
