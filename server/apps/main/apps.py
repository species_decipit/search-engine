# -*- coding: utf-8 -*-

import dramatiq
import nltk
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from django.apps import AppConfig
from django.conf import settings

from server.apps.main.tasks import parse_and_index_article


class MainConfig(AppConfig):
    """Class sets main django app configuration."""

    name = 'server.apps.main'

    def ready(self) -> None:
        """Method runs after setup main django app."""
        nltk.download('punkt')
        nltk.download('wordnet')
        nltk.download('stopwords')

        dramatiq.set_broker(settings.DRAMATIQ_BROKER)

        scheduler = BackgroundScheduler()
        scheduler.add_job(
            parse_and_index_article.send,
            CronTrigger.from_crontab('*/10 * * * *'),
        )
        scheduler.start()
