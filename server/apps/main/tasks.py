# -*- coding: utf-8 -*-

import os
from glob import glob

import dramatiq
import feedparser
from django.conf import settings
from goose3 import Goose

from server.apps.main.data_provider.data_provider import (
    restore_inverted_index,
    restore_prefix_tree,
    restore_soundex_index,
    dump_inverted_index,
    dump_prefix_tree,
    dump_soundex_index,
)
from server.apps.main.logic.parser.parser import index_document

broker = settings.DRAMATIQ_BROKER

RSS_URL: str = 'http://feeds.bbci.co.uk/news/world/rss.xml'


def generate_document_id() -> int:
    """Method calculates next document ID.

    Returns:
        Next document ID

    """
    return (
        max(
            map(
                lambda path: int(path.split('/')[-1]),
                glob(str(settings.MEDIA_ROOT / settings.DOCUMENT_PATH / '*')),
            )
        )
        + 1
    )


def store_document(document: str, document_id: int) -> None:
    """Method stores document in file system.

    Args:
        document: Document text
        document_id: Document ID

    """
    open(
        settings.MEDIA_ROOT / settings.DOCUMENT_PATH / str(document_id), 'w'
    ).write(document)


def is_indexed(link: str) -> bool:
    """Method checks whether article is already indexed or not/

    Args:
        link: Article link

    Returns:
        True if article is indexed, otherwise False

    """

    return (
        os.path.exists(settings.DOCUMENTS_LIST_FILE_NAME)
        and link in open(settings.DOCUMENTS_LIST_FILE_NAME, 'r').readlines()
    )


def add_link_to_list(link: str) -> None:
    """Method adds a link to list of documents.

    Args:
        link: Link to add

    """
    mode = 'a' if os.path.exists(settings.DOCUMENTS_LIST_FILE_NAME) else 'w'
    with open(settings.DOCUMENTS_LIST_FILE_NAME, mode) as documents:
        documents.write(f'{link}\n')


@dramatiq.actor(broker=broker)
def parse_and_index_article():
    """Method retrieves articles using RSS and indexes its."""
    inverted_index = restore_inverted_index(settings.PATH_INVERTED_INDEX_DUMP)
    prefix_tree = restore_prefix_tree(settings.PATH_PREFIX_TREE_DUMP)
    inverted_prefix_tree = restore_prefix_tree(
        settings.PATH_INVERTED_PREFIX_TREE_DUMP
    )
    soundex_index = restore_soundex_index(settings.PATH_SOUNDEX_INDEX_DUMP)

    articles = feedparser.parse(RSS_URL)
    for article in articles.entries:
        if not is_indexed(article.link):
            try:
                document: str = Goose().extract(article.link).cleaned_text
                document_id = generate_document_id()
                store_document(document, document_id)
                index_document(
                    document,
                    document_id,
                    inverted_index,
                    prefix_tree,
                    inverted_prefix_tree,
                    soundex_index,
                )
                add_link_to_list(article.link)
            except Exception as exception:
                print(exception)

        dump_inverted_index(inverted_index, settings.PATH_INVERTED_INDEX_DUMP)
        dump_prefix_tree(prefix_tree, settings.PATH_PREFIX_TREE_DUMP)
        dump_prefix_tree(
            inverted_prefix_tree, settings.PATH_INVERTED_PREFIX_TREE_DUMP
        )
        dump_soundex_index(soundex_index, settings.PATH_SOUNDEX_INDEX_DUMP)
