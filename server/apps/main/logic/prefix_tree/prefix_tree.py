# -*- coding: utf-8 -*-

from typing import Set, Union, List, Dict

IS_TERMINAL = 'is_terminal'


class PrefixTree(object):
    def __init__(self):
        """Method inits empty tree"""
        self.root = {IS_TERMINAL: False}

    def add_word(self, word: str) -> None:
        """Public method adds word in tree

        Args:
            word: Word

        """
        self._add_word(self.root, word)

    def find(self, word: str) -> Set[str]:
        """Public method finds all occurrences starting with word

        Args:
            word: Prefix word

        Returns:
            Set of occurrences start with word

        """
        results = []
        starting_position = self._find_start_position(self.root, word)
        if starting_position:
            self._find(starting_position, '', results)
            return set(word + result for result in results)
        else:
            return set()

    def _find(self, cursor: Dict, postfix: str, results: List[str]) -> None:
        """Private recursive method finds all terminals sub nodes.

        Args:
            cursor: Current node
            postfix: Accumulative postfix
            results: Final results

        """
        if cursor[IS_TERMINAL]:
            results.append(postfix)
        for key in cursor.keys():
            if key != IS_TERMINAL:
                self._find(cursor[key], postfix + key, results)

    def _add_word(self, cursor: Dict, word: str) -> None:
        """Private recursive method adds word in tree

        Args:
            cursor: Current node
            word: Left word

        """
        if not word:
            cursor[IS_TERMINAL] = True
            return
        else:
            token = word[0]
            if token not in cursor.keys():
                cursor[token] = {IS_TERMINAL: False}
                self._add_word(cursor[token], word[1:])
            else:
                self._add_word(cursor[token], word[1:])

    def _find_start_position(
        self, cursor: Dict, word: str
    ) -> Union[None, Dict]:
        """Private recursive method finds start position.

        Args:
            cursor: Current node
            word: Prefix word

        Returns:
            Start position

        """
        if not word:
            return cursor
        else:
            token = word[0]
            if token not in cursor.keys():
                return None
            else:
                return self._find_start_position(cursor[token], word[1:])


def find_with_wildcard(
    wildcard: str, pt: PrefixTree, ipt: PrefixTree
) -> List[str]:
    """Method finds all matched words.

    Args:
        wildcard: Token with wildcard
        pt: Instance of PrefixTree
        ipt: Instance of InvertedPrefixTree

    Returns:
        List of matched words

    """
    left, right = wildcard.split('*')
    return list(
        pt.find(left)
        & set([inverted[::-1] for inverted in ipt.find(right[::-1])])
    )
