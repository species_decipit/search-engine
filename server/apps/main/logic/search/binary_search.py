# -*- coding: utf-8 -*-

from typing import List

from server.apps.main.logic.inverted_index.inverted_index import InvertedIndex
from server.apps.main.logic.prefix_tree.prefix_tree import PrefixTree
from server.apps.main.logic.preprocessing.query_preprocessing import (
    preprocess_query,
)
from server.apps.main.logic.soundex_index.soundex_index import SoundexIndex


def search(
    inverted_index: InvertedIndex,
    prefix_tree: PrefixTree,
    inverted_prefix_tree: PrefixTree,
    soundex_index: SoundexIndex,
    query: str,
) -> List[int]:
    """Method performs search.

    Args:
        inverted_index: Instance of InvertedIndex
        prefix_tree: Instance of PrefixTree
        inverted_prefix_tree: Instance of PrefixTree (Inverted)
        soundex_index: Instance of SoundexIndex
        query: Raw query

    Returns:
        Set of documents IDs

    """
    preprocessed_tokens = preprocess_query(
        inverted_index, prefix_tree, inverted_prefix_tree, soundex_index, query
    )
    or_lists = [
        inverted_index.or_search(token)
        for token in preprocessed_tokens
        if isinstance(token, list)
    ]

    and_result = inverted_index.and_search(
        list(
            filter(
                lambda token: not isinstance(token, list), preprocessed_tokens
            )
        )
    )
    or_result = set.intersection(*or_lists) if or_lists else set()

    return (
        list(or_result & and_result)
        if or_result and and_result
        else and_result
        if and_result
        else or_result
    )
