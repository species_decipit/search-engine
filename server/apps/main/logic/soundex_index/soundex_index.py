# -*- coding: utf-8 -*-

from typing import Dict, Set, List

from jellyfish import soundex, levenshtein_distance


class SoundexIndex(object):
    """Class implements soundex index logic."""

    def __init__(self) -> None:
        """Method inits empty index."""
        self.index: Dict[str, Set[str]] = {}

    def add_token(self, word: str) -> str:
        """Method adds token in index.

        Args:
            word: Word

        Returns:
            Soundex value

        """
        soundex_value = soundex(word)
        if self.index.get(soundex_value, None):
            self.index[soundex_value].add(word)
        else:
            self.index[soundex_value] = {word}

        return soundex_value

    def get_the_most_closest(self, token: str) -> List[str]:
        """Method finds the most closest words to given token

        Args:
            token: Input token (most likely with type mistake)

        Returns:
            List of the potential tokens

        """
        soundex_value = soundex(token)
        words = self.index.get(soundex_value, None)
        if not words:
            return []
        else:
            pairs = [
                (word, levenshtein_distance(token, word)) for word in words
            ]
            min_distance = min([pair[1] for pair in pairs])
            return [pair[0] for pair in pairs if pair[1] == min_distance]
