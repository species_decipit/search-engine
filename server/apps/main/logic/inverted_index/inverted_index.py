# -*- coding: utf-8 -*-

from typing import List, Dict, Set


class InvertedIndex(object):
    """Class implements inverted index logic."""

    def __init__(self) -> None:
        """Method inits empty index"""
        self.index: Dict[str, Set[int]] = {}

    def or_search(self, preprocessed_tokens: List[str]) -> Set[int]:
        """Method performs search in inverted index using logical OR.

        Args:
            preprocessed_tokens: List of preprocessed tokens

        Returns:
            Set of documents IDs

        """
        result: Set[int] = set()
        for token in preprocessed_tokens:
            result |= self.index[token]
        return result

    def and_search(self, preprocessed_tokens: List[str]) -> Set[int]:
        """Method performs search in inverted index using logical AND.

        Args:
            preprocessed_tokens: List of preprocessed tokens

        Returns:
            Set of documents IDs

        """
        sets: List[Set[int]] = []
        for token in preprocessed_tokens:
            sets.append(self.index[token])
        return set.intersection(*sets) if sets else set()

    def add_token(self, token: str, doc_id: int) -> None:
        """Method adds token to index

        Args:
            token: Token
            doc_id: Document ID

        """
        if self.index.get(token, None):
            self.index[token].add(doc_id)
        else:
            self.index[token] = {doc_id}
