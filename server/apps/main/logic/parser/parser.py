# -*- coding: utf-8 -*-

from typing import Set

from server.apps.main.logic.inverted_index.inverted_index import InvertedIndex
from server.apps.main.logic.prefix_tree.prefix_tree import PrefixTree
from server.apps.main.logic.preprocessing.pipeline import preprocess
from server.apps.main.logic.soundex_index.soundex_index import SoundexIndex


def index_document(
    document: str,
    document_id: int,
    inverted_index: InvertedIndex,
    prefix_tree: PrefixTree,
    inverted_prefix_tree: PrefixTree,
    soundex_index: SoundexIndex,
) -> None:
    """Method performs indexing of document.

    Args:
        document: Document (just str)
        document_id: Document ID
        inverted_index: Instance of InvertedIndex
        prefix_tree: Instance of PrefixTree
        inverted_prefix_tree: Instance of PrefixTree (Inverted)
        soundex_index: Instance of SoundexIndex

    """
    tokens: Set[str] = preprocess(document)
    for token in tokens:
        inverted_index.add_token(token, document_id)
        prefix_tree.add_word(token)
        inverted_prefix_tree.add_word(token)
        soundex_index.add_token(token)
