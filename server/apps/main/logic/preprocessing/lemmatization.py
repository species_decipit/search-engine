# -*- coding: utf-8 -*-

from typing import List

from nltk.stem import WordNetLemmatizer


def lemmatize(tokens: List[str]) -> List[str]:
    """Method performs lemmatization list of tokens.

    Args:
        tokens: List of tokens

    Returns:
        List of limmatized tokens

    """
    lemmatizer = WordNetLemmatizer()
    return [lemmatizer.lemmatize(token) for token in tokens]
