# -*- coding: utf-8 -*-

from typing import Set

from server.apps.main.logic.preprocessing.lemmatization import lemmatize
from server.apps.main.logic.preprocessing.tokenization import tokenize
from server.apps.main.logic.preprocessing.normalization import normalize
from server.apps.main.logic.preprocessing.stop_words import remove_stop_word


def preprocess(text: str) -> Set[str]:
    """Method performs text preprocessing.

    Args:
        text: Raw text

    Returns:
        Preprocessed list of tokens

    """
    return set(remove_stop_word(lemmatize(tokenize(normalize(text)))))
