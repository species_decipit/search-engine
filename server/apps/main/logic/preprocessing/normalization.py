# -*- coding: utf-8 -*-

import re


def normalize(text: str) -> str:
    """Method normalizes text - lower case, non-word characters etc.

    Args:
        text: String with text

    Returns:
        Normalized text

    """
    return re.sub(r'[^a-zA-Z* ]', '', text.lower())
