# -*- coding: utf-8 -*-

from typing import List

from nltk.corpus import stopwords


def remove_stop_word(tokens: List[str]) -> List[str]:
    """Method removes stop words from tokens.

    Args:
        tokens: List of tokens

    Returns:
        List of tokens without stop words.

    """
    stop_words = set(stopwords.words('english'))
    return [token for token in tokens if token not in stop_words]
