# -*- coding: utf-8 -*-

from typing import List

import nltk


def tokenize(text: str) -> List[str]:
    """Method splits text in tokens.

    Args:
        text: String with text

    Returns:
        List of tokens

    """
    return nltk.word_tokenize(text)
