# -*- coding: utf-8 -*-

from typing import List

from django.conf import settings

from server.apps.main.logic.inverted_index.inverted_index import InvertedIndex
from server.apps.main.logic.prefix_tree.prefix_tree import (
    PrefixTree,
    find_with_wildcard,
)
from server.apps.main.logic.preprocessing.pipeline import preprocess
from server.apps.main.logic.soundex_index.soundex_index import SoundexIndex


def preprocess_query(
    inverted_index: InvertedIndex,
    prefix_tree: PrefixTree,
    inverted_prefix_tree: PrefixTree,
    soundex_index: SoundexIndex,
    query: str,
) -> List[str]:
    """Method preprocessed query in list of tokens.

    Args:
        inverted_index: instance of InvertedIndex
        prefix_tree: instance of PrefixTree
        inverted_prefix_tree: instance of PrefixTree (Inverted)
        soundex_index: instance of SoundexIndex
        query: Raw query

    Returns:
        List of preprocessed tokens

    """
    tokens = preprocess(query)
    result = []
    for token in tokens:
        if settings.WILDCARD_SIGN in token:
            result.append(
                find_with_wildcard(token, prefix_tree, inverted_prefix_tree)
            )
        else:
            if token in inverted_index.index:
                result.append(token)
            else:
                result.append(soundex_index.get_the_most_closest(token))
    return result
