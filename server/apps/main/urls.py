# -*- coding: utf-8 -*-

from django.urls import path

from server.apps.main.api.views.search import binary_search

# Place your URLs here:

app_name = 'main'

urlpatterns = [
    path('binary-search', binary_search, name='binary_search'),
]
