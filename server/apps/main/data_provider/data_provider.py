# -*- coding: utf-8 -*-

import json

from server.apps.main.logic.inverted_index.inverted_index import InvertedIndex
from server.apps.main.logic.prefix_tree.prefix_tree import PrefixTree
from server.apps.main.logic.soundex_index.soundex_index import SoundexIndex


def restore_inverted_index(path: str) -> InvertedIndex:
    """Method restores InvertedIndex instance from dump.

    Args:
        path: Path to dump

    Returns:
        Restored instance of InvertedIndex

    """
    inverted_index = InvertedIndex()
    inverted_index.index = {
        index: set(docs) for index, docs in json.load(open(path, 'r')).items()
    }
    return inverted_index


def dump_inverted_index(inverted_index: InvertedIndex, path: str) -> None:
    """Method dumps instance of inverted index.

    Args:
        inverted_index: Instance of InvertedIndex
        path: Path

    Returns:

    """
    json.dump(
        {
            doc_id: list(tokens)
            for doc_id, tokens in inverted_index.index.items()
        },
        open(path, 'w'),
    )


def restore_prefix_tree(path: str) -> PrefixTree:
    """Method restores the prefix tree from dump.

    Args:
        path: Path to dump

    Returns:
        Restored instance of PrefixTree

    """
    prefix_tree = PrefixTree()
    prefix_tree.root = json.load(open(path, 'r'))
    return prefix_tree


def dump_prefix_tree(prefix_tree: PrefixTree, path: str) -> None:
    """Method dumps instance of prefix tree.

    Args:
        prefix_tree: Instance of PrefixTree
        path: Path

    Returns:

    """
    json.dump(prefix_tree.root, open(path, 'w'))


def restore_soundex_index(path: str) -> SoundexIndex:
    soundex_index = SoundexIndex()
    soundex_index.index = {
        index: set(tokens)
        for index, tokens in json.load(open(path, 'r')).items()
    }
    return soundex_index


def dump_soundex_index(soundex_index: SoundexIndex, path: str) -> None:
    """Method dumps instance of soundex index.

    Args:
        soundex_index: Instance of SoundexIndex
        path: Path

    Returns:

    """
    json.dump(
        {value: list(token) for value, token in soundex_index.index.items()},
        open(path, 'w'),
    )
