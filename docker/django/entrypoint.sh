#!/usr/bin/env sh

set -o errexit
set -o nounset

cmd="$*"

postgres_ready () {
  # Check that postgres is up and running on port `5432`:
  dockerize -wait 'tcp://postgres:5432' -timeout 5s
}

redis_ready () {
  # Check that redis is up and running on port `6379`:
  dockerize -wait 'tcp://redis:6379' -timeout 5s
}

rabbitmq_ready () {
  # Check that redis is up and running on port `15672`:
  dockerize -wait 'tcp://rabbitmq:15672' -timeout 5s
  dockerize -wait 'tcp://rabbitmq:5672' -timeout 5s
}



# We need this line to make sure that this container is started
# after the one with postgres:
until postgres_ready; do
  >&2 echo 'Postgres is unavailable - sleeping'
done
>&2 echo 'Postgres is up - continuing...'

until rabbitmq_ready; do
  >&2 echo 'RabbitMQ is unavailable - sleeping'
done
>&2 echo 'RabbitMQ is up - continuing...'


# It is also possible to wait for other services as well: redis, elastic, mongo

# Evaluating passed command (do not touch):
# shellcheck disable=SC2086
python manage.py runserver 0.0.0.0:8000 & dramatiq server.apps.main.tasks:broker -v -p 1 -t 1
